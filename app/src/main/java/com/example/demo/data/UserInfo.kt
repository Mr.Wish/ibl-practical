package com.example.demo.data

class UserInfo {
    var email: String? = null
    var password: String? = null
    var dob: String? = null
    var gender: String? = null
    var full_name: String? = null
    var imageUrl: String? = null
}