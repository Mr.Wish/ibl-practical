package com.example.demo.prefs

import android.content.Context


class PreferenceDataHelper private constructor(context: Context) {
    private val mSharedPreferenceHelper: SharedPreferenceHelper

    companion object {
        private const val IS_USER_LOGGED_IN = "user_logged_in"
        private var sInstance: PreferenceDataHelper? = null

        @Synchronized
        fun getInstance(context: Context): PreferenceDataHelper? {
            if (sInstance == null) {
                sInstance = PreferenceDataHelper(context)
            }
            return sInstance
        }
    }

    init {
        SharedPreferenceHelper.initialize(context)
        mSharedPreferenceHelper = SharedPreferenceHelper.getsInstance()!!
    }

    /* fun setUser(user: UserResponse) {
         val userString: String = Gson().toJson(user, UserResponse::class.java)
         mSharedPreferenceHelper.setString(USER, userString)
     }

     fun getUser(): UserResponse {
         return Gson().fromJson(mSharedPreferenceHelper.getString(USER), UserResponse::class.java)
     }
 */
    fun geUserLoggedIn(): Boolean {
        return mSharedPreferenceHelper.getBoolean(IS_USER_LOGGED_IN, false)
    }

    fun setUserLoggedIn() {
        mSharedPreferenceHelper.setBoolean(IS_USER_LOGGED_IN, true)
    }


    fun clearUserSpecificPreferences() {
        mSharedPreferenceHelper.removeKey(IS_USER_LOGGED_IN)
    }


}