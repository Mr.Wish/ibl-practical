package com.example.demo.ui.view

import com.example.demo.base.MVPBaseView
import com.example.demo.ui.presenter.RegisterPresenter

interface RegisterView : MVPBaseView<RegisterPresenter> {
    fun initViews()
    fun showToast(id: Int)
    fun showProgressBar(b: Boolean)
    fun onSignUpSuccess()
}