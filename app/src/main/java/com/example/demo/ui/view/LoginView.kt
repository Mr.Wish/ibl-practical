package com.example.demo.ui.view

import com.example.demo.base.MVPBaseView
import com.example.demo.ui.presenter.LoginPresenter

interface LoginView : MVPBaseView<LoginPresenter> {
    fun initViews()
    fun showToast(id: Int)
    fun showProgressBar(b: Boolean)
    fun onLoginSuccess()
}