package com.example.demo.ui.view

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.demo.R
import com.example.demo.base.BaseFragment
import com.example.demo.prefs.PreferenceDataHelper
import com.example.demo.ui.presenter.RegisterPresenter
import com.example.demo.utils.AppConstants
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.fragment_register.*
import java.io.ByteArrayOutputStream
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : BaseFragment<RegisterPresenter>(),
    RegisterView {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    private var isImageSelected: Boolean = false
    var mYear = 0
    var mMonth = 0
    var mDay = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun initViews() {
        btnSignup.setOnClickListener {
            val gender =
                if (radioMale.isChecked) AppConstants.MALE else if (radioFemale.isChecked) AppConstants.FEMALE else ""
            if (!isImageSelected) {
                showToast(R.string.select_image)
            } else if (presenter.isFieldValid(
                    etEmail.text.toString().trim(),
                    etPassword.text.toString().trim(),
                    etName.text.toString().trim(),
                    etBirthDate.text.toString().trim(),
                    gender
                )
            ) {
                uploadImage()
            }
        }
        initDates()
        etBirthDate.setOnClickListener { v: View? ->
            val datePickerDialog = DatePickerDialog(
                activity!!,
                { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                    etBirthDate.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                    mYear = year
                    mMonth = monthOfYear
                    mDay = dayOfMonth
                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()
        }
        tvLogin.setOnClickListener { activity?.onBackPressed() }
        rlUser.setOnClickListener { openGallery() }
    }

    private fun uploadImage() {
        showProgressBar(true)
        val bitmap = (ivAvatar.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data: ByteArray = baos.toByteArray()
        val storage = FirebaseStorage.getInstance()
        // path temp set
        val storageRef: StorageReference = storage.reference.child(Random().nextInt().toString())
        val uploadTask: UploadTask = storageRef.putBytes(data)
        uploadTask.addOnFailureListener {
            showToast(R.string.error_occur_in_signup)
            showProgressBar(false)
        }.addOnSuccessListener {
            val imageUrl = storageRef.downloadUrl.toString()
            val gender =
                if (radioMale.isChecked) AppConstants.MALE else if (radioFemale.isChecked) AppConstants.FEMALE else ""
            presenter.signUp(
                etEmail.text.toString().trim(),
                etPassword.text.toString().trim(),
                etName.text.toString().trim(),
                etBirthDate.text.toString().trim(),
                gender,
                imageUrl.toString()
            )
        }
    }

    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val uri = data?.data
            ivAvatar.setImageURI(uri)
            isImageSelected = true
        }
    }

    override fun showToast(id: Int) {
        Toast.makeText(activity, id, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar(b: Boolean) {
        progressBar.visibility = if (b) View.VISIBLE else View.GONE
        btnSignup.isEnabled = !b
    }

    override fun onSignUpSuccess() {
        PreferenceDataHelper.getInstance(activity!!)!!.setUserLoggedIn()
        MainActivity.start(activity!!)
    }

    private fun initDates() {
        val c = Calendar.getInstance()
        mYear = c.get(Calendar.YEAR)
        mMonth = c.get(Calendar.MONTH)
        mDay = c.get(Calendar.DAY_OF_MONTH)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            RegisterFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun getNewPresenter(): RegisterPresenter {
        return RegisterPresenter()
    }

    override fun getScreenName(): String {
        return RegisterFragment.javaClass.simpleName.toString()
    }
}