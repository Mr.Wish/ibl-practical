package com.example.demo.ui.presenter

import com.example.demo.R
import com.example.demo.base.MVPBasePresenter
import com.example.demo.ui.view.LoginView
import com.example.demo.utils.AppConstants
import com.google.firebase.firestore.FirebaseFirestore

class LoginPresenter : MVPBasePresenter<LoginView>() {
    private lateinit var mFirebase: FirebaseFirestore
    fun onViewCreated() {
        view.initViews()
        mFirebase = FirebaseFirestore.getInstance()
    }

    fun login(email: String, password: String) {
        if (isFieldValid(email, password)) {
            view.showProgressBar(true)
            mFirebase.collection(AppConstants.USERS)
                .whereEqualTo(AppConstants.EMAIL, email)
                .whereEqualTo(AppConstants.PASSWORD, password).get()
                .addOnSuccessListener { documents ->
                    if (!isViewAlive) return@addOnSuccessListener
                    view.showProgressBar(false)
                    for (document in documents) {
                        view.showToast(R.string.login_success)
                        view.onLoginSuccess()
                        return@addOnSuccessListener
                    }
                    view.showToast(R.string.user_not_exists)
                }.addOnFailureListener {
                    if (!isViewAlive) return@addOnFailureListener
                    view.showProgressBar(false)
                }
        }
    }

    private fun isFieldValid(email: String, password: String): Boolean {
        if (email.isEmpty()) {
            view.showToast(R.string.enter_email)
            return false
        }
        if (password.isEmpty()) {
            view.showToast(R.string.enter_password)
            return false
        }
        if (password.length < 6) {
            view.showToast(R.string.enter_password_limit)
            return false
        }
        return true
    }
}