package com.example.demo.ui.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import com.example.demo.prefs.PreferenceDataHelper
import com.example.demo.ui.adapter.MyAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class MainActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        ivLogout.setOnClickListener {
            PreferenceDataHelper.getInstance(this)!!.clearUserSpecificPreferences()
            OnboardingActivity.start(this)
        }
        initTabs()
    }

    private fun initTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("Quotes"))
        tabLayout.addTab(tabLayout.newTab().setText("Images"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter =
            MyAdapter(supportFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(TabLayoutOnPageChangeListener(tabLayout))


    }
}