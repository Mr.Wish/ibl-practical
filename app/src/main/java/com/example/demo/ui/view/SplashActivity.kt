package com.example.demo.ui.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import com.example.demo.prefs.PreferenceDataHelper

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initViews()
    }

    private fun initViews() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (PreferenceDataHelper.getInstance(this)!!.geUserLoggedIn())
                MainActivity.start(this)
            else
                OnboardingActivity.start(this)
        }, 2000)
    }
}