package com.example.demo.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.demo.R

class QuotesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qoutes, container, false)
    }

    companion object {
        fun newInstance() =
            QuotesFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }

    fun callAPI() {
    }
}