package com.example.demo.ui.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.demo.R

class OnboardingActivity : AppCompatActivity() {
    companion object {
        fun start(context: Context) {
            val intent = Intent(context, OnboardingActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
        initViews()
    }

    private fun initViews() {
        openFragment(LoginFragment.newInstance())
    }

    private fun openFragment(fragment: Fragment, isBackStack: Boolean = false) {
        if (isBackStack) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .addToBackStack(null)
                .commit()

        } else {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .commit()
        }
    }

    fun openSignUpFragment() {
        openFragment(RegisterFragment.newInstance(), true)
    }
}