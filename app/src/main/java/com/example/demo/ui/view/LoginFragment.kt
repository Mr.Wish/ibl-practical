package com.example.demo.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.demo.R
import com.example.demo.base.BaseFragment
import com.example.demo.prefs.PreferenceDataHelper
import com.example.demo.ui.presenter.LoginPresenter
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment<LoginPresenter>(),
    LoginView {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewCreated()
    }

    override fun initViews() {
        tvSignUpLogin.setOnClickListener { (activity as OnboardingActivity).openSignUpFragment() }
        btnLogin.setOnClickListener {
            presenter.login(
                etEmail.text.toString().trim(),
                etPassword.text.toString().trim()
            )
        }
    }

    override fun showToast(id: Int) {
        Toast.makeText(activity, id, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar(b: Boolean) {
        progressBar.visibility = if (b) View.VISIBLE else View.GONE
        btnLogin.isEnabled = !b
    }

    override fun onLoginSuccess() {
        PreferenceDataHelper.getInstance(activity!!)!!.setUserLoggedIn()
        MainActivity.start(activity!!)
    }

    companion object {
        fun newInstance() =
            LoginFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    override fun getNewPresenter(): LoginPresenter {
        return LoginPresenter()
    }

    override fun getScreenName(): String {
        return LoginFragment.javaClass.simpleName.toString()
    }
}