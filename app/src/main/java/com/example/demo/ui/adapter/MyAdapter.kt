package com.example.demo.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.demo.ui.view.ImageFragment
import com.example.demo.ui.view.QuotesFragment

@Suppress("DEPRECATION")
class MyAdapter(
    fm: FragmentManager?,
    var totalTabs: Int
) : FragmentPagerAdapter(fm!!) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                QuotesFragment.newInstance()
            }
            1 -> {
                ImageFragment.newInstance()
            }
            else -> QuotesFragment.newInstance()
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

}