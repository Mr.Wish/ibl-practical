package com.example.demo.ui.presenter

import com.example.demo.R
import com.example.demo.base.MVPBasePresenter
import com.example.demo.data.UserInfo
import com.example.demo.ui.view.RegisterView
import com.example.demo.utils.AppConstants
import com.google.firebase.firestore.FirebaseFirestore

class RegisterPresenter : MVPBasePresenter<RegisterView>() {
    private lateinit var mFirebase: FirebaseFirestore
    fun onViewCreated() {
        view.initViews()
        mFirebase = FirebaseFirestore.getInstance()
    }

    fun signUp(
        email: String,
        password: String,
        full_name: String,
        dob: String,
        gender: String,
        imageUrl: String
    ) {

        val user = UserInfo()
        user.email = email
        user.password = password
        user.full_name = full_name
        user.dob = dob
        user.gender = gender
        user.imageUrl = imageUrl
        view.showProgressBar(true)
        mFirebase.collection(AppConstants.USERS).whereEqualTo(AppConstants.EMAIL, email).get()
            .addOnSuccessListener { snapshot ->
                if (!isViewAlive) return@addOnSuccessListener
                for (document in snapshot) {
                    view.showToast(R.string.email_exists)
                    view.showProgressBar(false)
                    return@addOnSuccessListener
                }
                mFirebase.collection(AppConstants.USERS)
                    .add(user)
                    .addOnSuccessListener {
                        if (!isViewAlive) return@addOnSuccessListener
                        view.showToast(R.string.sign_up_success)
                        view.onSignUpSuccess()
                        view.showProgressBar(false)
                    }
                    .addOnFailureListener { e ->
                        if (!isViewAlive) return@addOnFailureListener
                        view.showToast(R.string.error_occur_in_signup)
                        view.showProgressBar(false)
                    }
            }.addOnFailureListener {
                if (!isViewAlive) return@addOnFailureListener
                view.showToast(R.string.error_occur_in_signup)
                view.showProgressBar(false)
            }

    }

    fun isFieldValid(
        email: String,
        password: String,
        fullName: String,
        dob: String,
        gender: String
    ): Boolean {
        if (email.isEmpty()) {
            view.showToast(R.string.enter_email)
            return false
        }
        if (password.isEmpty()) {
            view.showToast(R.string.enter_password)
            return false
        }
        if (password.length < 6) {
            view.showToast(R.string.enter_password_limit)
            return false
        }
        if (fullName.isEmpty()) {
            view.showToast(R.string.enter_full_name)
            return false
        }
        if (dob.isEmpty()) {
            view.showToast(R.string.enter_dob)
            return false
        }
        if (gender.isEmpty()) {
            view.showToast(R.string.enter_gender)
            return false
        }
        return true
    }

}