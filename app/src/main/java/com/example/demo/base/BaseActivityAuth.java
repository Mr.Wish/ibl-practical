package com.example.demo.base;

/**
 * Created on 27/10/16.
 */

import android.os.Bundle;

import androidx.annotation.Nullable;

/**
 * Base class for activities that need authentication
 */
public abstract class BaseActivityAuth<P extends MVPBasePresenter> extends BaseActivity<P> {

//    private PreferenceDataHelper preferenceDataHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // check login access token from preference
//        preferenceDataHelper = PreferenceDataHelper.getInstance(getApplicationContext());

    }
}
