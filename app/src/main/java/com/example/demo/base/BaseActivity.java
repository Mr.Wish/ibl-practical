package com.example.demo.base;

/**
 * Created on 27/10/16.
 */

/**
 * BaseActivity for all activities
 * - Has all common functionalites needed in all acitivities
 * eg - App Manager
 */
public abstract class BaseActivity<P extends MVPBasePresenter> extends MVPBaseActivity<P> {
}
