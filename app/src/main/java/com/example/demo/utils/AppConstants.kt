package com.example.demo.utils

class AppConstants {

    companion object {
        const val MALE = "male"
        const val FEMALE = "female"
        const val USERS = "users"
        const val EMAIL = "email"
        const val PASSWORD = "password"
    }
}